//
//  VerRegistrosViewController.swift
//  trabajoiOSFrontEnd
//
//  Created by user1 on 9/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import Alamofire

class VerRegistrosViewController: UIViewController, UITableViewDataSource {

    
    @IBOutlet weak var registroTableView: UITableView!
    var contenido=[[String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bm=BackendManager()
        bm.verRegistros()
        
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("object"), object: nil)
        
                // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        registroTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return contenido.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        cell.textLabel?.text=contenido[indexPath.section][indexPath.row+1]
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Nombre: "+contenido[section][0]
    }
    
    
   
    
    func actualizarDatos(_ notification:Notification){
        let json=notification.userInfo?["object"] as! NSArray
        
        self.contenido=[]
        
        for i in 0..<json.count{
            let dict=json[i] as! NSDictionary
            self.contenido.append(["\(dict["nombre"]!)","\(dict["apellido"]!)","\(dict["edad"]!)","\(dict["correo"]!)"])
        }
    
registroTableView.reloadData()
      

        
    }
   
}
