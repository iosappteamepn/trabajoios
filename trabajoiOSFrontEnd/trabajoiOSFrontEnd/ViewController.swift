//
//  ViewController.swift
//  trabajoiOSFrontEnd
//
//  Created by alexfb on 25/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var apellidoTextField: UITextField!
    @IBOutlet weak var edadTextField: UITextField!
    @IBOutlet weak var correoTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func registrarseButton(_ sender: Any) {
        let backend=BackendManager()
        let parametro:[String:Any]=["nombre":nombreTextField.text!,"apellido":apellidoTextField.text!,"edad":edadTextField.text!,"correo":correoTextField.text!]
        backend.registrarDatosBase(parametro)
        NotificationCenter.default.addObserver(self, selector: #selector(desplegarMensaje), name: NSNotification.Name("ok"), object: nil)
        
    }
    func desplegarMensaje(_ notification:Notification){
        
        let mostrarMensaje=notification.userInfo?["ok"]
        
        
        if String(describing: mostrarMensaje!) == "1" {
            print("Funciono el programa")
            let mensaje1=UIAlertController(title: "Mensaje", message: "Los datos fueron almacenados correctamente.", preferredStyle: UIAlertControllerStyle.alert)
            mensaje1.addAction(UIAlertAction(title:"Aceptar",style: UIAlertActionStyle.default))
            self.present(mensaje1,animated: true,completion: nil)
            
            
            
        }else{
            print("Hubo un error")
            let mensaje2=UIAlertController(title: "Mensaje", message: "Hubo un error, intente nuevamente.", preferredStyle: UIAlertControllerStyle.alert)
            mensaje2.addAction(UIAlertAction(title:"Aceptar",style: UIAlertActionStyle.default))
            self.present(mensaje2,animated: true,completion: nil)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

