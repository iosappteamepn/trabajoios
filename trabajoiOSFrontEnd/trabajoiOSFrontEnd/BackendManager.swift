//
//  BackendManager.swift
//  trabajoiOSFrontEnd
//
//  Created by user1 on 1/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire

class BackendManager {
 
    func registrarDatosBase(_ parametros:[String:Any]){
        let urlString="http://localhost:1337/"
        Alamofire.request(urlString+"Registro",method:.post, parameters: parametros).responseJSON { response in
            debugPrint(response)
            
            if let json = response.result.value {
               
                NotificationCenter.default.post(name: NSNotification.Name("ok"), object: nil, userInfo: ["ok" : "1"])
            }else{
                print("error")
                NotificationCenter.default.post(name: NSNotification.Name("ok"), object: nil, userInfo: ["ok" : "0"])
            }
        
        }
        
    }
    
    func verRegistros(){
        let urlString="http://localhost:1337/"
        Alamofire.request(urlString+"Registro").responseJSON { response in
            debugPrint(response)
            
            if let json = response.result.value {
                

                NotificationCenter.default.post(name: NSNotification.Name("object"), object: nil, userInfo: ["object" : json])
                
            }else{
                print("error")
                NotificationCenter.default.post(name: NSNotification.Name("ok"), object: nil, userInfo: ["ok" : "0"])
            }
            
        }
        
    }
 
}
